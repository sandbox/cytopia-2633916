Version 0.3 (unreleased)
-----------


Version 0.2
-----------

- [Fea]  Implemented `check_drupal_log`
- [Fix]  Fixed misspelled variables
- [Enh]  First check all arguments, then run the checks
- [Enh]  Move scripts to bin directory
- [Enh]  Doc directory for more documentation


Version 0.1
-----------

- [Fea]  Check for Drupal security updates
- [Fea]  Check for Drupal system updates
- [Fea]  Check for Drupal required database updates
- [Fea]  Check for Drupal core errors
- [Fea]  Check for Drupal core warnings
- [Fea]  Every check can specify nagios severity (Error or Warning)
- [Fea]  Specify custom name for nagios short output
- [Fea]  Be able to successfully recognize valid Drupal6 or Drupal7 document root
- [Fea]  Detailed information in nagios long output
- [Fea]  Basic performance data fow: how many OKs, Errors, Warnings and Unknowns
